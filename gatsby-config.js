// in gatsby-config.js
module.exports = {
    plugins: [
      `gatsby-plugin-sass`,
      {
        resolve: "gatsby-plugin-react-svg",
        options: {
          rule: {
            include: /assets/ // See below to configure properly
          }
        }
      },
    `gatsby-transformer-json`,
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `card`,
          path: `${__dirname}/src/data`,
        },
      },
    ],
    
  }
