import React from 'react';

import H2 from "./h2";
import IcoAttach from "./icon/attachment";
import IcoComment from "./icon/comments";
import IcoUser from "./icon/user";
import AddTask from "./addTask";

import { StaticQuery, graphql } from "gatsby"

const CardList = () => (
  <StaticQuery
    query={graphql`
      {
        allDataJson {
          edges {
            node {
              status
              card {
                id
                priority
                content
                comments
                attachment
                user
              }
            }
          }
        }
      }
    `}
    
   
    
    render={data  => (

      <section className="c-dashboard">
      <H2/>
      <section className="c-dashboard--rails">
        {data.allDataJson.edges.map(item => {
          return (
            <section key={item.node.status} className={"c-cards--list--" + item.node.status}>
              {item.node.card.map((cardItem) => {
                return (
                    <article key={cardItem.id} id={cardItem.id} className={"c-card--item--" + cardItem.id}>
                      <h3 className="c-card--title">{item.node.status}</h3>
                      <div className="c-card--body">
                        <div className="c-task" data-cloneable="true">
                            <h4 className="c-task--status">{cardItem.priority}</h4>
                            <div className="c-task--message">{cardItem.content}</div>
                            <ul className="c-task--actions">
                                <li className="c-task--comments">
                                  <IcoComment/>{cardItem.comments}
                                </li>
                                <li className="c-task--adj">
                                  <IcoAttach/>{cardItem.attachment}
                                </li>
                                <li className="c-task--user">
                                  <IcoUser/>
                                </li>
                            </ul>
                        </div>    
                      </div>
                      
                        <AddTask/>
                      
                    </article>
                )
              })}
             </section>
          )
        })}
        </section>
      </section>
    )}
  />
)
export default CardList