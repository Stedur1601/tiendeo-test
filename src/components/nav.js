import React, { Component } from "react"
import IcoManage from "../components/icon/manage"
import IcoTasks from "../components/icon/tasks"
import IcoSchedule from "../components/icon/schedule"
import IcoReports from "../components/icon/reports"
import IcoSettings from "../components/icon/settings"

class Nav extends Component {
  render() {
    
    return (
      <div>
        <nav id="menu" className="c-nav">
        <ul className="c-nav--list">
            <li className="c-nav--item"><a href="/" className="c-nav--link" title="Manage" tabIndex="1"><IcoManage/> <span>Manage</span></a></li>
            <li  className="c-nav--item"><a href="/" className="c-nav--link" title="Tasks" tabIndex="2"><IcoTasks/> <span>Tasks</span></a></li>
            <li  className="c-nav--item"><a href="/" className="c-nav--link" title="Shedule" tabIndex="3"><IcoSchedule/> <span>Shedule</span></a></li>
            <li  className="c-nav--item"><a href="/" className="c-nav--link" title="Reports" tabIndex="4"><IcoReports/> <span>Reports</span></a></li>
            <li  className="c-nav--item"><a href="/" className="c-nav--link" title="Settings" tabIndex="5"><IcoSettings/> <span>Settings</span></a></li>
        </ul>
    </nav>
      </div>
    );
  }
}

export default Nav;