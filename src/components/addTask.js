import React from "react"
import IcoAddTasks from "../components/icon/addTasks"
import IcoAttach from "./icon/attachment";
import IcoComment from "./icon/comments";
import IcoUser from "./icon/user";

class AddTasks extends React.Component{
    constructor(){
        super();

        this.state = {
            children: []
        }
    }

    appendChild(){

        this.setState({
            children: [
                
                <div className="c-card--body">
                    <div className="c-task">
                        <h4 className="c-task--status">aaa</h4>
                        <div className="c-task--message"></div>

                        <ul className="c-task--actions">
                            <li className="c-task--comments">
                            <IcoComment/>
                            </li>
                            <li className="c-task--adj">
                            <IcoAttach/>
                            </li>
                            <li className="c-task--user">
                            <IcoUser/>
                            </li>
                        </ul>
                    </div>    
                </div>
            ]
        });
    }

    render(){
        return(
            <div className="c-newblock">
               
                {this.state.children.map(child => child)}
                 
                <footer className="c-card--addtask">
                    <button  onClick={() => this.appendChild()} id="c_addbutton" className="c-card--add--cta" name="add-task" aria-label="add-task">
                        <span className="c-card--add--text">Add task</span><IcoAddTasks/>
                    </button>
                </footer>
            </div>
        );
    }
}

export default AddTasks;


/*
export default props =>  (
    <button id="c_addbutton" className="c-card--add--btn"><span className="c-card--add--text">Add task</span><IcoAddTasks/></button>
)
*/