import React from "react"

import IcoAdd from "../components/icon/add"
export default props =>  (
    <button className="c-task--add--cta" name="add-card" aria-label="add-card"><IcoAdd/></button>
)