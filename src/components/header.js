import React, { Component } from "react"
import IcoMenu from "./icon/menu";
import IcoSearch from "./icon/search";
import IcoNotification from "./icon/notification";
import IcoUser from "./icon/user";

class Header extends Component {
    render() {
        return (
            <header className="o-header">
                <div className="c-header--logo">
                    <button className="c-menu--cta" onClick={this.props.setToggleTopMenuClass} name="show-menu" aria-label="show-menu">
                        <IcoMenu/>
                    </button>
                    <div className="c-brand">
                        <h1>TaskSky</h1>
                    </div>
                </div>
                <div className="c-header--searcher">
                    <button className="c-search--cta" name="search" aria-hidden="true"><IcoSearch/></button>
                    <input type="search" placeholder="Search fo tasks..." className="c-input--searcher"/>
                </div>
                <div className="c-header--notifications">
                    <button className="c-notification--cta" name="show-notifications" aria-label="show-notifications">
                        <IcoNotification/>
                    </button>
                </div>
                <div className="c-header--user">
                        <div className="c-user--name">
                        Mr Thompson
                        </div>
                        <div className="c-user--avatar">
                            <IcoUser/>
                        </div>
                </div>
            </header>
        )
    }
}
export default Header;