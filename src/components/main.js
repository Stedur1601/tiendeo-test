import React from "react"


import CardList from "./cardList";
import AddCard from "./addCard";

export default props =>  (
    <main className="o-main">
        
        <CardList/>
        <AddCard/>
</main>
)