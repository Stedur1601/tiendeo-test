import React, { Component } from "react"
import Header from "../components/header"

import Nav from "../components/nav"
import CardList2 from "../components/cardList2";
import AddCard from "../components/addCard";

import '../styles/style.scss'

class App extends Component {
    constructor(props) {
        super(props);

       this.state = {
            menu_class: '',
        }
        this.setToggleTopMenuClass = this.setToggleTopMenuClass.bind(this);


    }


    setToggleTopMenuClass = function() {
        if (this.state.menu_class === '') {
            this.setState({
                menu_class: 'is_Open',
            })
        } else {
            this.setState({
                menu_class: '',
            })
        }

    }

    render() {
        let top_menu_class = `o-content ${this.state.menu_class}`

        return (
            <div className={top_menu_class}>

            <Header setToggleTopMenuClass={this.setToggleTopMenuClass}/>
            <main className="o-main">
                <Nav/>
                <CardList2/>
                <AddCard/>
            </main>
        </div>
        );
    }
}
export default App;
/*
function classToggle() {
    const navs = document.querySelectorAll('.c-nav')
    
    navs.forEach(nav => nav.classList.toggle('is_Open'));
  }
  
  
document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('.c-menu--cta').addEventListener('click', classToggle);
});*/